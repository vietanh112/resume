import py_vncorenlp
import posixpath as p
from rich import print

# Automatically download VnCoreNLP components from the original repository
# and save them in some local machine folder
vncorenlp_dir = './vncorenlp'
vncorenlp_dir = p.realpath(vncorenlp_dir)

py_vncorenlp.download_model(save_dir=vncorenlp_dir)

# Load the word and sentence segmentation component
rdrsegmenter = py_vncorenlp.VnCoreNLP(annotators=["wseg"], save_dir=vncorenlp_dir)
# rdrsegmenter = py_vncorenlp.VnCoreNLP(save_dir=vncorenlp_dir)
text = "Ông Nguyễn Khắc Chúc  đang làm việc tại Đại học Quốc gia Hà Nội. Bà Lan, vợ ông Chúc, cũng làm việc tại đây."

output = rdrsegmenter.word_segment(text)

print(output)
# ['Ông Nguyễn_Khắc_Chúc đang làm_việc tại Đại_học Quốc_gia Hà_Nội .', 'Bà Lan , vợ ông Chúc , cũng làm_việc tại đây .']