import torch
from rich import print
from copy import deepcopy
from transformers import AutoModel, AutoTokenizer, RobertaConfig, RobertaModel, BertPreTrainedModel

# phobert = AutoModel.from_pretrained("vinai/phobert-base")
# tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base")

# print(phobert.config)
# cfg = RobertaConfig.from_pretrained("vinai/phobert-base")
phobert = RobertaModel.from_pretrained("vinai/phobert-base")
print(phobert)
# print(isinstance(phobert.config, RobertaConfig))
# print(phobert['last_hidden_state'] - phobert['last_hidden_state'])
# # Initializing a RoBERTa configuration
# configuration = RobertaConfig()

# # Initializing a model from the configuration
# model = RobertaModel(configuration)
# # print(model)
# # Accessing the model configuration
# configuration = model.config
# print(configuration)

