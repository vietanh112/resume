import re
from string import punctuation


pattern = '[{}]'.format(punctuation)
def remove_special_character(text:str) -> str:
    r"""
    xử lý loại bỏ các ký tự đặc biệt trong text
    ví dụ EMA::IL: -> email
    """
    return re.sub(pattern, '', text.lower(), count=len(text)).strip()

