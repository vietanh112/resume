from typing import List
from transformers import RobertaModel, AutoTokenizer
from transformers.modeling_outputs import BaseModelOutputWithPoolingAndCrossAttentions as OwPnCA
import torch, py_vncorenlp
import posixpath as p
import os

class PhoBERT_with_vncoreNLP(object):
    def __init__(self, vncorenlp_dir:str = './vncorenlp') -> None:
        self.phobert = RobertaModel.from_pretrained("vinai/phobert-base")
        self.tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base")
        
        self.vncoreNLP_dir = p.realpath(vncorenlp_dir)
        if not p.isdir(self.vncoreNLP_dir):
            os.makedirs(self.vncoreNLP_dir)
            
        py_vncorenlp.download_model(save_dir=self.vncoreNLP_dir)
        self.rdrsegmenter = py_vncorenlp.VnCoreNLP(annotators=["wseg"], 
                                                   save_dir=self.vncoreNLP_dir)
        
    
    def create_token(self, rawtext:str) -> List[str]:
        r"""
        rawtext là str, là một đoạn văn hoặc 1 câu
        return một list, mỗi elem là 1 câu đã đc token
        
        rawtext = "Ông Nguyễn Khắc Chúc  đang làm việc tại Đại học Quốc gia Hà Nội. Bà Lan, vợ ông Chúc, cũng làm việc tại đây."

        return [
            'Ông Nguyễn_Khắc_Chúc đang làm_việc tại Đại_học Quốc_gia Hà_Nội .', 
            'Bà Lan , vợ ông Chúc , cũng làm_việc tại đây .'
            ]
        """
        return self.rdrsegmenter.word_segment(rawtext)
        
    @torch.no_grad()
    def create_word_embeded(self, token:str):
        input_ids = torch.tensor([self.tokenizer.encode(token)])
        features:OwPnCA = self.phobert(input_ids)  # Models outputs are now tuples
        # return features
        return features['pooler_output'].view(-1)
        

if __name__ == "__main__":
    from rich import print
    model = PhoBERT_with_vncoreNLP()
    text = "Ông Nguyễn Khắc Chúc  đang làm việc tại Đại học Quốc gia Hà Nội. Bà Lan, vợ ông Chúc, cũng làm việc tại đây."
    tokens = model.create_token(text)
    print()
    print(tokens)
    print()
    
    # for token in tokens:
    #     ft = model.create_word_embeded(token)
    #     print(ft, '\n')